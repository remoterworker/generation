
package Project1;


// Clase Auto
public class Auto {
    private String modelo;
    private String color;
    private int annio;
    private Marca marca;
    private String chasis;
    private Propietario propietario;
    private int velocidadMaxima;
    private int velocidadActual;
    private int numeroPuertas;
    private boolean tieneTechoSolar;
    private int numeroMarchas;
    private boolean tieneTransmisionAutomatica;

    public Auto(String modelo, String color, int annio, Marca marca, String chasis, Propietario propietario,
                int velocidadMaxima, int numeroPuertas, boolean tieneTechoSolar, int numeroMarchas,
                boolean tieneTransmisionAutomatica) {
        this.modelo = modelo;
        this.color = color;
        this.annio = annio;
        this.marca = marca;
        this.chasis = chasis;
        this.propietario = propietario;
        this.velocidadMaxima = velocidadMaxima;
        this.numeroPuertas = numeroPuertas;
        this.tieneTechoSolar = tieneTechoSolar;
        this.numeroMarchas = numeroMarchas;
        this.tieneTransmisionAutomatica = tieneTransmisionAutomatica;
        this.velocidadActual = 0; // La velocidad inicial es 0
    }
    
    
    // Getters para velocidadActual y numeroMarchas
    public int getVelocidadActual() {
        return velocidadActual;
    }

    public int getNumeroMarchas() {
        return numeroMarchas;
    }
    // Métodos de aceleración, frenado, cambio de marcha, reducción de marcha, autonomía de viaje.
    public void acelerar() {
        if (velocidadActual < velocidadMaxima) {
            velocidadActual++;
        } else {
            System.out.println("Ya se alcanzó la velocidad máxima.");
        }
    }

    public void frenar() {
        velocidadActual = 0;
    }

    public void cambiarMarcha(int nuevaMarcha) {
        if (nuevaMarcha < 0 || (nuevaMarcha == -1 && velocidadActual > 0)) {
            // No se puede engranar marcha atrás si la velocidad es mayor que 0 km/h
            System.out.println("No se puede engranar marcha atrás con el vehículo en movimiento.");
        } else {
            // Implementación del cambio de marcha
            numeroMarchas = nuevaMarcha;
        }
    }

    public void reducirMarcha() {
        if (numeroMarchas > 0) {
            numeroMarchas--;
        } else {
            System.out.println("Ya se encuentra en la marcha más baja.");
        }
    }

    public double calcularAutonomia(double consumoMedio, double cantidadCombustible) {
        // Implementación del cálculo de la autonomía
        // Se calcula la autonomía como la cantidad de combustible disponible dividida por el consumo medio
        return cantidadCombustible / consumoMedio;
    }

    public double mostrarVolumenCombustible(double nivelCombustible, double capacidadTanque) {
        // Implementación del método para mostrar el volumen de combustible
        // Se calcula el volumen de combustible como el nivel de combustible actual en relación con la capacidad total del tanque
        return (nivelCombustible / capacidadTanque) * 100; // Se devuelve el porcentaje de llenado del tanque
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }
}
