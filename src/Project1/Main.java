
package Project1;


// Clase principal para probar el funcionamiento
public class Main {
    public static void main(String[] args) {
        // Creacion de objetos de las clases necesarias
        Direccion direccionPropietario = new Direccion("Calle Principal", "Centro", "Ciudad A");
        Propietario propietario = new Propietario("Juan", "01/01/1990", direccionPropietario);
        Marca marcaAuto = new Marca("Toyota", 5, 2005, "T1");
        Auto auto = new Auto("Corolla", "Negro", 2020, marcaAuto, "12345", propietario,
                200, 4, false, 6, true);
// Mostrar los detalles del propietario
        System.out.println("Detalles del Propietario:");
        System.out.println("Nombre: " + propietario.getNombre());
        System.out.println("Fecha de Nacimiento: " + propietario.getFechaNacimiento());
        System.out.println("Dirección: " + propietario.getDireccion().getCiudad()+"/"+propietario.getDireccion().getBarrio()+"/"+propietario.getDireccion().getCalle());
// Mostrar algunos detalles del Auto
        System.out.println("Auto Marca:"+auto.getMarca().getNombre());
        System.out.println("Auto Modelo:"+auto.getModelo());
        System.out.println("Auto Marca:"+auto.getColor());
       
    }
}

