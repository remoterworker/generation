
package Project1;

// Clase Marca
public class Marca {
    private String nombre;
    private int nrDeModelos;
    private int annioLanzamiento;
    private String codigoIdentificador;

    public Marca(String nombre, int nrDeModelos, int annioLanzamiento, String codigoIdentificador) {
        this.nombre = nombre;
        this.nrDeModelos = nrDeModelos;
        this.annioLanzamiento = annioLanzamiento;
        this.codigoIdentificador = codigoIdentificador;
    }

    // Getters y Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNrDeModelos() {
        return nrDeModelos;
    }

    public void setNrDeModelos(int nrDeModelos) {
        this.nrDeModelos = nrDeModelos;
    }

    public int getAñoLanzamiento() {
        return annioLanzamiento;
    }

    public void setAñoLanzamiento(int annioLanzamiento) {
        this.annioLanzamiento = annioLanzamiento;
    }

    public String getCodigoIdentificador() {
        return codigoIdentificador;
    }

    public void setCodigoIdentificador(String codigoIdentificador) {
        this.codigoIdentificador = codigoIdentificador;
    }
}
