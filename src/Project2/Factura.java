
package Project2;

public class Factura {
    private String numero;
    private String descripcion;
    private int cantidad;
    private double precio;

    // Constructor de la clase Factura
    public Factura(String numero, String descripcion, int cantidad, double precio) {
        this.numero = numero;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    // Métodos getter y setter para cada variable de instancia
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        if (precio > 0) {
            this.precio = precio;
        } else {
            this.precio = 0.0; // Si el precio no es positivo, se establece en 0.0
        }
    }

    // Método para calcular el monto de la factura
    public double getTotalFactura() {
        double total = cantidad * precio;
        if (total > 0) {
            return total;
        } else {
            return 0.0; // Si el total no es positivo, se establece en 0.0
        }
    }
}
