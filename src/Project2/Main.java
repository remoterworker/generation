
package Project2;

public class Main {
    public static void main(String[] args) {
        // Crear una instancia de Factura
        Factura factura1 = new Factura("001", "Teclado", 2, 15.5);

        // Mostrar los detalles de la factura
        System.out.println("Detalles de la Factura:");
        System.out.println("Número: " + factura1.getNumero());
        System.out.println("Descripción: " + factura1.getDescripcion());
        System.out.println("Cantidad: " + factura1.getCantidad());
        System.out.println("Precio: " + factura1.getPrecio());
        System.out.println("Total de la factura: " + factura1.getTotalFactura());
    }
}

