
package Project3;

// Clase EmpleadoTest para probar la clase Empleado
public class EmpleadoTest {
    public static void main(String[] args) {
        // Crear dos objetos Empleado
        Empleado empleado1 = new Empleado("Juan", "Perez", 3000);
        Empleado empleado2 = new Empleado("Maria", "Gomez", 4000);

        // Mostrar el salario mensual de cada empleado
        System.out.println("Salario mensual de " + empleado1.getNombre() + " " + empleado1.getApellido() + ": $" + empleado1.getSalarioMensual());
        System.out.println("Salario mensual de " + empleado2.getNombre() + " " + empleado2.getApellido() + ": $" + empleado2.getSalarioMensual());

        // Aplicar un aumento del 10% a cada empleado
        empleado1.setSalarioMensual(empleado1.getSalarioMensual() * 1.10);
        empleado2.setSalarioMensual(empleado2.getSalarioMensual() * 1.10);

        // Mostrar el salario anual de cada empleado después del aumento
        System.out.println("\nSalario anual de " + empleado1.getNombre() + " " + empleado1.getApellido() + " después del aumento: $" + empleado1.calcularSalarioAnual());
        System.out.println("Salario anual de " + empleado2.getNombre() + " " + empleado2.getApellido() + " después del aumento: $" + empleado2.calcularSalarioAnual());

        // Mostrar el número total de empleados
        System.out.println("\nTotal de empleados: " + Empleado.getContadorEmpleados());
    }
}
