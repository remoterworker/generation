
package Project4;


public class IntegerSet {
    private boolean[] conjunto; // Matriz para representar el conjunto

    // Constructor sin argumentos para inicializar el conjunto vacío
    public IntegerSet() {
        conjunto = new boolean[101]; // Tamaño de la matriz para números enteros de 0 a 100
    }

    // Método para realizar la unión de dos conjuntos y devolver el resultado
    public IntegerSet union(IntegerSet otroConjunto) {
        IntegerSet resultado = new IntegerSet();
        for (int i = 0; i < 101; i++) {
            resultado.conjunto[i] = this.conjunto[i] || otroConjunto.conjunto[i];
        }
        return resultado;
    }

    // Método para realizar la intersección de dos conjuntos y devolver el resultado
    public IntegerSet interseccion(IntegerSet otroConjunto) {
        IntegerSet resultado = new IntegerSet();
        for (int i = 0; i < 101; i++) {
            resultado.conjunto[i] = this.conjunto[i] && otroConjunto.conjunto[i];
        }
        return resultado;
    }

    // Método para insertar un elemento en el conjunto
    public void insertElement(int elemento) {
        if (elemento >= 0 && elemento <= 100) {
            conjunto[elemento] = true;
        }
    }

    // Método para eliminar un elemento del conjunto
    public void deleteElement(int elemento) {
        if (elemento >= 0 && elemento <= 100) {
            conjunto[elemento] = false;
        }
    }

    // Método para obtener una representación del conjunto como una cadena
    public String toSetString() {
        StringBuilder sb = new StringBuilder();
        boolean vacio = true;
        for (int i = 0; i < 101; i++) {
            if (conjunto[i]) {
                sb.append(i).append(" ");
                vacio = false;
            }
        }
        if (vacio) {
            return "-";
        }
        return sb.toString().trim();
    }

    // Método para determinar si dos conjuntos son iguales
    public boolean equalTo(IntegerSet otroConjunto) {
        for (int i = 0; i < 101; i++) {
            if (this.conjunto[i] != otroConjunto.conjunto[i]) {
                return false;
            }
        }
        return true;
    }
    
    // Método para determinar si un elemento está presente en el conjunto
    public boolean contains(int elemento) {
        if (elemento >= 0 && elemento <= 100) {
            return conjunto[elemento];
        }
        return false;
    }
}

