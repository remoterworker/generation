
package Project4;


public class Main {
    public static void main(String[] args) {
        // Crear dos conjuntos de prueba
        IntegerSet conjunto1 = new IntegerSet();
        IntegerSet conjunto2 = new IntegerSet();

        // Insertar elementos en los conjuntos
        conjunto1.insertElement(10);
        conjunto1.insertElement(20);
        conjunto1.insertElement(30);

        conjunto2.insertElement(20);
        conjunto2.insertElement(30);
        conjunto2.insertElement(40);
        conjunto2.insertElement(50);

        // Mostrar los conjuntos
        System.out.println("Conjunto 1: " + conjunto1.toSetString());
        System.out.println("Conjunto 2: " + conjunto2.toSetString());

        // Realizar la unión de los conjuntos
        IntegerSet union = conjunto1.union(conjunto2);
        System.out.println("Unión de conjuntos: " + union.toSetString());

        // Realizar la intersección de los conjuntos
        IntegerSet interseccion = conjunto1.interseccion(conjunto2);
        System.out.println("Intersección de conjuntos: " + interseccion.toSetString());

        // Verificar si los conjuntos son iguales
        System.out.println("¿Los conjuntos son iguales? " + conjunto1.equalTo(conjunto2));
    }
}

