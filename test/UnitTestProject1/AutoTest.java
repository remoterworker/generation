package UnitTestProject1;


import Project1.Auto;
import static org.junit.Assert.assertEquals;
import org.junit.Test;



public class AutoTest {
    
    // Prueba unitaria para el método de acelerar
    @Test
    public void testAcelerar() {
        Auto auto = new Auto("Corolla", "Negro", 2020, null, "12345", null,
                200, 4, false, 6, true);
        
        // Se prueba acelerar el auto
        auto.acelerar();
        assertEquals(1, auto.getVelocidadActual()); // Se espera que la velocidad actual sea 1
    }
    
    // Prueba unitaria para el método de frenar
    @Test
    public void testFrenar() {
        Auto auto = new Auto("Corolla", "Negro", 2020, null, "12345", null,
                200, 4, false, 6, true);
        
        // Se establece una velocidad inicial
        auto.acelerar();
        auto.acelerar();
        
        // Se prueba frenar el auto
        auto.frenar();
        assertEquals(0, auto.getVelocidadActual()); // Se espera que la velocidad actual sea 0
    }
    
    // Prueba unitaria para el método de cambiarMarcha
    @Test
    public void testCambiarMarcha() {
        Auto auto = new Auto("Corolla", "Negro", 2020, null, "12345", null,
                200, 4, false, 6, true);
        
        // Se cambia la marcha del auto
        auto.cambiarMarcha(3);
        assertEquals(3, auto.getNumeroMarchas()); // Se espera que el número de marchas sea 3
    }
    
    // Prueba unitaria para el método de calcularAutonomia
    @Test
    public void testCalcularAutonomia() {
        Auto auto = new Auto("Corolla", "Negro", 2020, null, "12345", null,
                200, 4, false, 6, true);
        
        // Se establece el consumo medio y la cantidad de combustible
        double autonomia = auto.calcularAutonomia(10, 50); // Consumo medio de 10 km/l, 50 litros de combustible
        
        assertEquals(5, autonomia, 0.01); // Se espera que la autonomía sea 5 horas (50 litros / 10 km/l)
    }
    
    // Prueba unitaria para el método de mostrarVolumenCombustible
    @Test
    public void testMostrarVolumenCombustible() {
        Auto auto = new Auto("Corolla", "Negro", 2020, null, "12345", null,
                200, 4, false, 6, true);
        
        // Se establece el nivel de combustible y la capacidad del tanque
        double volumenCombustible = auto.mostrarVolumenCombustible(30, 60); // 30 litros de combustible, tanque de 60 litros
        
        assertEquals(50, volumenCombustible, 0.01); // Se espera que el tanque esté al 50% de su capacidad
    }
}


