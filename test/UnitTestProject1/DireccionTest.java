package UnitTestProject1;


import Project1.Direccion;
import static org.junit.Assert.assertEquals;
import org.junit.Test;



public class DireccionTest {
    
    // Prueba unitaria para el constructor de Direccion
    @Test
    public void testConstructorDireccion() {
        Direccion direccion = new Direccion("Calle Principal", "Centro", "Ciudad A");
        
        // Se verifica que los atributos se hayan asignado correctamente
        assertEquals("Calle Principal", direccion.getCalle());
        assertEquals("Centro", direccion.getBarrio());
        assertEquals("Ciudad A", direccion.getCiudad());
    }
}