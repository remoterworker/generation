package UnitTestProject1;


import Project1.Marca;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class MarcaTest {
    
    // Prueba unitaria para el constructor de Marca
    @Test
    public void testConstructorMarca() {
        Marca marca = new Marca("Toyota", 5, 2005, "T1");
        
        // Se verifica que los atributos se hayan asignado correctamente
        assertEquals("Toyota", marca.getNombre());
        assertEquals(5, marca.getNrDeModelos());
        assertEquals(2005, marca.getAñoLanzamiento());
        assertEquals("T1", marca.getCodigoIdentificador());
    }
}