package UnitTestProject1;


import Project1.Direccion;
import Project1.Propietario;
import static junit.framework.Assert.assertEquals;
import org.junit.Test;

public class PropietarioTest {
    
    // Prueba unitaria para el constructor de Propietario
    @Test
    public void testConstructorPropietario() {
        Direccion direccionPropietario = new Direccion("Calle Principal", "Centro", "Ciudad A");
        Propietario propietario = new Propietario("Juan", "01/01/1990", direccionPropietario);
        
        // Se verifica que los atributos se hayan asignado correctamente
        assertEquals("Juan", propietario.getNombre());
        assertEquals("01/01/1990", propietario.getFechaNacimiento());
        assertEquals(direccionPropietario, propietario.getDireccion());
    }
}
