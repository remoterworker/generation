
package UnitTestProject2;

import Project2.Factura;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class FacturaTest {
    
    // Prueba unitaria para el método getTotalFactura con valores positivos
    @Test
    public void testGetTotalFactura_PositiveValues() {
        Factura factura = new Factura("001", "Teclado", 2, 15.5);
        assertEquals(31.0, factura.getTotalFactura(), 0.01); // Se espera que el total de la factura sea 31.0
    }
    
    // Prueba unitaria para el método getTotalFactura con cantidad negativa
    @Test
    public void testGetTotalFactura_NegativeQuantity() {
        Factura factura = new Factura("002", "Ratón", -1, 10.0);
        assertEquals(0.0, factura.getTotalFactura(), 0.01); // Se espera que el total de la factura sea 0.0
    }
    
    // Prueba unitaria para el método getTotalFactura con precio negativo
    @Test
    public void testGetTotalFactura_NegativePrice() {
        Factura factura = new Factura("003", "Monitor", 1, -20.0);
        assertEquals(0.0, factura.getTotalFactura(), 0.01); // Se espera que el total de la factura sea 0.0
    }
    
    // Prueba unitaria para el método getTotalFactura con cantidad y precio negativos
    @Test
    public void testGetTotalFactura_NegativeQuantityAndPrice() {
        Factura factura = new Factura("004", "Impresora", -3, -50.0);
        assertEquals(0.0, factura.getTotalFactura(), 0.01); // Se espera que el total de la factura sea 0.0
    }
    
    // Prueba unitaria para el método getTotalFactura con cantidad y precio cero
    @Test
    public void testGetTotalFactura_ZeroQuantityAndPrice() {
        Factura factura = new Factura("005", "Disco Duro", 0, 0.0);
        assertEquals(0.0, factura.getTotalFactura(), 0.01); // Se espera que el total de la factura sea 0.0
    }
}
