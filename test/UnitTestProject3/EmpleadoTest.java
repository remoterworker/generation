
package UnitTestProject3;

import Project3.Empleado;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class EmpleadoTest {

    // Prueba unitaria para verificar el cálculo del salario anual
    @Test
    public void testCalcularSalarioAnual() {
        // Crear un empleado con salario mensual de $3000
        Empleado empleado = new Empleado("Juan", "Perez", 3000);

        // Calcular el salario anual esperado (3000 * 12)
        double salarioAnualEsperado = 3000 * 12;

        // Obtener el salario anual real del empleado
        double salarioAnualReal = empleado.calcularSalarioAnual();

        // Verificar si el salario anual calculado es igual al esperado
        assertEquals(salarioAnualEsperado, salarioAnualReal, 0.01); // Usamos un delta de 0.01 para manejar errores de redondeo
    }

    // Prueba unitaria para verificar el aumento del salario
    @Test
    public void testAumentarSalario() {
        // Crear un empleado con salario mensual de $4000
        Empleado empleado = new Empleado("Maria", "Gomez", 4000);

        // Aplicar un aumento del 10%
        empleado.setSalarioMensual(empleado.getSalarioMensual() * 1.10);

        // Calcular el salario anual esperado después del aumento (4000 * 1.10 * 12)
        double salarioAnualEsperado = 4000 * 1.10 * 12;

        // Obtener el salario anual real del empleado después del aumento
        double salarioAnualReal = empleado.calcularSalarioAnual();

        // Verificar si el salario anual después del aumento es igual al esperado
        assertEquals(salarioAnualEsperado, salarioAnualReal, 0.01); // Usamos un delta de 0.01 para manejar errores de redondeo
    }
}
