
package UnitTestProject4;

import Project4.IntegerSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class IntegerSetTest {
    private IntegerSet conjunto1;
    private IntegerSet conjunto2;

    @Before
    public void setUp() {
        // Configuración inicial para las pruebas
        conjunto1 = new IntegerSet();
        conjunto2 = new IntegerSet();
        conjunto1.insertElement(10);
        conjunto1.insertElement(20);
        conjunto1.insertElement(30);

        conjunto2.insertElement(20);
        conjunto2.insertElement(30);
        conjunto2.insertElement(40);
        conjunto2.insertElement(50);
    }

    @Test
    public void testUnion() {
        // Prueba de la unión de conjuntos
        IntegerSet union = conjunto1.union(conjunto2);
        assertEquals("10 20 30 40 50", union.toSetString());
    }

    @Test
    public void testInterseccion() {
        // Prueba de la intersección de conjuntos
        IntegerSet interseccion = conjunto1.interseccion(conjunto2);
        assertEquals("20 30", interseccion.toSetString());
    }

    @Test
    public void testInsertElement() {
        // Prueba de inserción de un elemento en un conjunto
        conjunto1.insertElement(40);
        assertTrue(conjunto1.contains(40));
    }

    @Test
    public void testDeleteElement() {
        // Prueba de eliminación de un elemento en un conjunto
        conjunto1.deleteElement(20);
        assertFalse(conjunto1.contains(20));
    }

    @Test
    public void testToSetString() {
        // Prueba de obtener una representación de cadena del conjunto
        assertEquals("10 20 30", conjunto1.toSetString());
    }

    @Test
    public void testEqualTo() {
        // Prueba de igualdad entre dos conjuntos
        assertFalse(conjunto1.equalTo(conjunto2));
    }
}
